# Ubicycling #
===========

JIRA                   : https://ubiprism.atlassian.net/browse/UD

Jenkins Project        : https://jenkins.ubiprism.pt/view/Demo-Training%20/job/Ubicycling/

QEM Functional Reviews : https://beubi.qem.io/ubicyclingtr/reviews

"Production" Server    : https://ubicycling.ubiprism.pt/

Toggles admin          : https://toggleadmin.ubicycling.ubiprism.pt



### Development Environment

To spin up a full replica of the infrastructure:
```
#!sh
docker-compose up -d
```


###### Load fixtures:

In order to have some workouts to play around you can load some with the following command

```
#!bash
docker-compose run console doctrine:fixtures:load
```



### Testing

#### Run Acceptance Tests (system level)
```
#!sh
docker-compose exec web bin/behat
```

In order to test a specific feature you can append the feature to the end of the command.

You can also append the line number to test a specific scenario.

```
#!sh
docker-compose exec web bin/behat features/afeature.feature:24
```


#### Run Unit Tests (unit & integration level)
```
#!sh
docker-compose run phpunit -c /srv/project/app
```

In order to test a specific unit test you can append `--filter <name of the test>` e.g. ` --filter verifying_project_mandatory_fields`

You can also append the path to the directory that has several tests to run them all e.g. `docker-compose run phpunit -c /srv/project/app src/Beubi/DemoBundle/Tests/A_Test_Folder`