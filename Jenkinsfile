#! groovy

def developPoll = ''
def masterPoll = ''
branch = scm.branches[0].toString()

def prodKey = '~/.ssh/logserver.pem'
def prodConnection = 'ubuntu@52.49.158.48'

pipeline {
    agent { label 'Docker' }
    environment {
        COMPOSE_PROJECT_NAME = "ubicycling-training.${env.BRANCH_NAME}.${env.BUILD_NUMBER}"
    }
    options {
        ansiColor('xterm')
    }
    triggers { pollSCM(branch == 'master' ? masterPoll : developPoll) }

    stages {
        stage('Boot') {
            steps {
                script {
                    sh "docker-compose up -d"
                    sh "docker-compose run web sfWaitDb"
                    sh "docker-compose exec -T web composer install --no-interaction"
                    sh "docker-compose run console doctrine:schema:update --force"
                    sh "docker-compose run console doctrine:fixtures:load --no-interaction"
                }
            }
        }
        stage('Unit Tests') {
            steps {
                script {
                    sh "docker-compose run phpunit -c /srv/project/app"
                }
            }
        }
        stage('Acceptance Tests') {
            steps {
                script {
                    sh "docker-compose exec -T web /srv/project/bin/behat"
                }
            }
        }
        stage('Deploy') {
            when { expression { return branch == 'develop' || branch == 'master' } }
            steps {
                script {
                    node("master") {
                        sh 'ssh -i ' + prodKey + ' ' + prodConnection + ' "exec ssh-agent bash -c \\"ssh-add /home/ubuntu/.ssh/id_rsa_ubicycling-training && cd /srv/ubicycling-training && git checkout docker-compose.yml && git pull && ssh-agent -k \\" ; "'
                        sh 'ssh -i ' + prodKey + ' ' + prodConnection + ' "cd /srv/ubicycling-training && cp docker-compose-prod.yml docker-compose.override.yml"'
                        sh 'ssh -i ' + prodKey + ' ' + prodConnection + ' "cd /srv/ubicycling-training && sed -i \'/ports:/,+1d\' ./docker-compose.yml"'
                        sh 'ssh -i ' + prodKey + ' ' + prodConnection + ' "cd /srv/ubicycling-training && docker-compose up -d"'
                        sh 'ssh -i ' + prodKey + ' ' + prodConnection + ' "cd /srv/ubicycling-training && docker-compose exec -T web composer install --no-interaction"'
                        sh 'ssh -i ' + prodKey + ' ' + prodConnection + ' "cd /srv/ubicycling-training && docker-compose run --rm console doctrine:schema:update --force"'
                        sh 'ssh -i ' + prodKey + ' ' + prodConnection + ' "cd /srv/ubicycling-training && docker-compose run --rm console featuretogglehelper:bootstrap-toggles"'
                        sh 'ssh -i ' + prodKey + ' ' + prodConnection + ' "sudo chmod 777 -R /srv/ubicycling-training/app/cache/"'
                        sh 'ssh -i ' + prodKey + ' ' + prodConnection + ' "sudo chmod 777 -R /srv/ubicycling-training/app/logs/"'
                    }
                    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: '5dc6424f-ddc0-4c73-b5c1-beef40db7263', usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD']]) {
                        sh "git fetch https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/beubi/ubicycling-training.git +refs/heads/develop:refs/remotes/origin/develop"
                        sh "git checkout develop"
                        sh "git pull"
                        def tagName = sh(script: 'date +%Y_%m_%d_%H_%M', returnStdout: true)
                        sh "git tag ${tagName}"
                        sh "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/beubi/ubicycling-training.git --follow-tags"
                    }
                }
            }
        }
    }
    post {
        always {
            sh 'rm -f docker-compose.override.yml'
            sh 'docker-compose down -v --remove-orphans --rmi local || true'
            sh 'docker-compose rm -fvs || true'
        }
    }
}
