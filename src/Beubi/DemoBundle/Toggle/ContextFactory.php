<?php

namespace Beubi\DemoBundle\Toggle;

use Qandidate\Toggle\ContextFactory as BaseContextFactory;
use Qandidate\Toggle\Context;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Beubi\DemoBundle\Entity\User;

/**
 * Instantiates context that might be relevant to decide
 *  if a feature should be toggled or not
 */
class ContextFactory extends BaseContextFactory
{
    /**
     * The token storage of current session to allow to retrieve
     *  session info
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * Allows a user to be available to provide user-related
     * context parameters
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @inheritdoc
     * @return Context a context automatically created from current session
     */
    public function createContext()
    {
        $context = new Context();

        return $context;
    }
}
