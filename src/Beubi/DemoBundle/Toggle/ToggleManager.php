<?php

namespace Beubi\DemoBundle\Toggle;

use Qandidate\Toggle\ToggleManager as BaseToggleManager;
use Qandidate\Toggle\ToggleCollection;
use Qandidate\Toggle\ContextFactory as BaseContextFactory;
use Qandidate\Toggle\Context;

/**
 * Extends Qandidate's toggle manager to automatically
 * inject a common context
 */
class ToggleManager extends BaseToggleManager
{
    /**
     * An instantiated class that automatically generates contexts when needed
     *
     * @var BaseContextFactory
     */
    private $contextFactory;

    /**
     * Creates a new toggle manager and makes context factory
     * available for later usage
     *
     * @param ToggleCollection   $collection     The list of toggles and their configurations
     * @param BaseContextFactory $contextFactory The context factory to inject for later automatic creation of context
     */
    public function __construct(ToggleCollection $collection, BaseContextFactory $contextFactory)
    {
        parent::__construct($collection);

        $this->contextFactory = $contextFactory;
    }

    /**
     * Checks whether a toggle should be considered active
     *
     * Extends base method by making providing a context
     * optional
     *
     * @param string       $name    The name of the toggle to check
     * @param Context|null $context The context to be used to determine toggle status
     *
     * @return bool True if the toggle should be active
     */
    public function active($name, Context $context = null)
    {
        if (null === $context) {
            $context = $this->contextFactory->createContext();
        }

        return parent::active($name, $context);
    }
}
