<?php

namespace Beubi\DemoBundle\Toggle;

/**
 * This class should contain all the toggles available so that, to get a toggle, devs can just
 *
 * \AppBundle\Toggle\Toggles::ToggleName
 */
class Toggles implements \FeatureToggleHelperBundle\Toggle\TogglesInterface
{

    const TOGGLES = [
        self::ENABLE_CONTACTS
    ];

    const ENABLE_CONTACTS = 'enable_contacts';

    /**
     * @return array
     */
    public function getToggles()
    {
        return self::TOGGLES;
    }
}
