Feature: Footer has link to be.ubi
  In order to know who's the author of this platform
  As a user
  I need to see a link to be.ubi on its footer

Scenario: Copyright is on the footer
  Given I am on the homepage
  Then I should see "Copyright ©" in the "footer" element
  And I should see "be.ubi" in the "footer" element

Scenario: be.ubi links to its website
  Given I am on the homepage
  And I press "be.ubi"
  Then I should be on "https://www.beubi.com"
